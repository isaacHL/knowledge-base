If you see what's in your mind clearly and understand the power your thoughts have, then you understand reality while everone else is running around confused and angry and upset because they think reality is something happening to them rather than something they are making every moment with every thought.

To talk to people and see the tunnels and vortices in their minds and to undestand the realities they would create if those thoughts led to action.

For my enemies, I was meant to bring about the inherent retribution flowing from the reality made by their own evil thoughts.

And for my friends, to protect them from their tendency to underestimate the power their confusion has over their fate... to keep them out of trouble.

Fortune is the essence of light, and it shines on those who've mastered it.


People would definitely nod and say, "Yep, that little lady sure did emerge from a womb on account of that gentleman's awesome virility." 



“In any given moment we have two options: to step
forward into growth or back into safety.”