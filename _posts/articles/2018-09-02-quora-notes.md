---
layout: post
title:  "Notes from Quora Articles taken on Saturday 04-08-2018"
date:   2018-09-02
categories: notes quora intp
---

#What is an INTP?

#### From: http://qr.ae/TUNw90

> According to the cognitive functions that provide the foundation of MBTI, an INTP is someone that uses Ti, Ne, Si, and Fe in their conscious cognitive function stack.

### Ti - Introverted thinking

> This is the INTP dominant function. TI is a decision making function that values logic, independent thought and information that is as accurate as possible.

> It builds an internal structure within the mind that is extremely valuable to its user for making sense of the world and how it works. 
> Since Ti is an introverted function, it means that INTPs feel most at home within their own heads, thinking about whatever it is that they find interesting. INTPs can be very good at using Ti to solve difficult problems or puzzles, generally giving them a reputation as intelligent, analytical people.

### Ne - Extroverted iNtuition

> It is considered the INTP’s auxiliary, parent, or co-pilot function. This means that it helps, serves, and provides some guidance to the first function, Ti. Ne does this as a perceiving, or information-gathering function, by looking for ideas and connections between ideas in the outside world.
> It can make mental connections between very different things to help aid understanding or create new and interesting possibilities from the interplay of these connections. Since Ne is extroverted, and the first extroverted function that an INTP uses, they often show an outer sense of a child-like curiosity, a questioning, seeking demeanor, always thirsty to learn and eager to speculate on what things might be and what they might imply beyond their current state.

### Si - Introverted Sensing

> The third position is called the tertiary, child, or 10-year old function. And here we cross the threshold of INTP strengths and start to see some INTP weaknesses. Si, being in the third slot, is not usually as mature or well-developed in INTPs.
> Si is a perceiving function, like Ne, but rather dealing with ideas in the outer world, it deals with detailed facts and personal impressions of facts from the inner world based on what is gathered through one’s senses of the real, physical world. It is informed by one’s own personal experiences in the past.
> Si looks to the past to see what is expected and informs the user when things do and do not align with these expectations. Combined with Ne, INTPs can sometimes see the flow of changes in systems over time and form speculations with Ne to try to make sense of them by using Ne connections and pattern-recognition, which inform Ti’s understanding. But since it is further down the cognitive function stack, Si is often less reliable than the INTP would often like it to be. INTPs have a reputation as absent-minded professors, head in the clouds and forgetting where they are, what time it is, and appointments they have committed to. They may become so focused internally that they forget to eat or delay taking care of other physical needs. This is in contrast to strong Si users that are often very disciplined and practical. INTPs can become this, but it takes some effort to get better at the Si details of life.

### Fe - Extroverted Feeling

> Fe is called the inferior or 3-year old, as it is the least developed conscious function, a point of great weakness but also an area seen as a growth area to aspire to.
>  Like Ti, it is a decision-making function. Unlike Ti, Fe is an outer function and can be rather expressive outwardly, both in caring behaviors and enforcing the social contracts and rules that bind society together for the common good. Since INTPs use Fe in their 4th slot, this can be a source of great pain, confusion, and difficulty for them. Depending on how well INTPs have practiced and developed their Fe, they may be completely oblivious to social rules, break them purposefully, or try to follow them but feel insecure and afraid of their lack of ability to do so in a natural way. Low Fe can also mean that INTPs may be uncomfortable with strong outer displays of feeling. Still, when they need to or feel it is socially appropriate, INTPs can be very warm and caring people, well-intentioned if occasionally awkward when trying to show it.