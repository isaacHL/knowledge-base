---
layout: post
title:  "Notes from Zell Liew Articles taken on Saturday 04-08-2018"
date:   2018-08-04
categories: notes zell-liew
---

# Notes from Zell Liew Articles

Extracted from https://tinyurl.com/y6v5bywl

### Beating Procrastination

#### Why People procrastinate

> We procrastinate for only one reason: **We are afraid**. We are afraid that something bad will happen to us if we fail. We are afraid that something will go wrong if we succeed. So we get stressed.

> Unfortunately, our brains are not built to handle this stress. Most of the time, our primal instincts take over and tell us to run.

> The stress we're feeling is mental. We want to run away from it, and it means running away from the source that caused it.

> That's the procrastination cycle. We get stressed, we run, the stress gets relieved, albeit temporarily.

> The cycle repeats itself when we're faced with important work... until you break it.

> We tend to cover up our fears with bullshit because the real fear hidden deep within is incredibly painful.

> Don't beat yourself up if you have to face the fear again. It happens. When you conquered it once, you know you can do it again.

#### Overcoming Procrastination

> Procrastination is a habit. It reinforces itself whenever a cycle is completed. This is why it becomes harder and harder to break procrastination the longer you've been doing it.

> **To break the procrastination habit, you have to be aware of what makes you begin procrastination. Once you notice the cue, you have to change your routine. Instead of procrastinating, you can choose to stick with the task and finish it. You need immense courage to look at what's really stopping you, and to push past it.

> What are you really afraid of? What are you really doing this for? 

> Dig deep, it takes real courage to find the real fear hidden deep within. If you don't find the fear, you'll need a reason to continue doing what you're doing. Find it, hold on to it, because it's the only thing what will keep you going.

#### Beating procrastination is a habit

> Once you beat procrastination for the first time, it gets easier to beat again, and again, and again. It's possible to turn it into a habit. It's possible to accomplish what's really important to you.

> **So stop procrastinating and get your arse moving. Procrastination gets stronger every day. Start beating it today. Start beating it now.

