---
layout: post
title:  "Notes from Quora Articles taken on Saturday 04-08-2018"
date:   2018-08-04
categories: notes quora
---

# Notes from Quora Articles

Extracted from: http://qr.ae/TUISGH

### How do I stop constantly overthinking and stressing about everything?

> The mind only works in one direction: more thinking.

> Clarity is the mind's default state.

> We tend to want to analyse, fix or cope with whatever problem we are faced with. What underpins this behaviour is fear - fear of a negative outcome.

> We are so afraid of failing, being rejected or being alone that our mind compulsively tries to analyse, fix or cope with the problem.

> Yet it is not the situation or circumstance that causes the overthinking - it is our fear of the negative outcome.

> The fear is not real, it is a fabrication of our mind that we create when we use our incredibly creative powers to imagine a negative, scary future, the negative future is not real either.

> What's happening is that we are creating an imaginary future which makes us uncomfortable and afraid and it pushes our mind into overdrive.

> One way to deal with it is to become emotionally ok with the idea of the negative consequence.

> Most of what we fear is the unknown. The idea of a particular negative outcome feels bad and uncomfortable that we do our best to never think about it. As a result, we continue being driven by fear and we keep overthinking.

> When we become okay with the fear, it loses its power over us. This allows our mind to naturally settle because we've neutralized the fear that was causing our anxiety and overthinking in the first place.

> The second way is to simply understand the mechanism that creates our reality.

> **When we learn to see that we are only living in the feeling of our thinking and not the feeling of our circumstances, our life transforms**

> When our mind overthinks, we sit back and enjoy the show.

> When we feel afraid, we allow that feeling fully without getting caught in its story.

> When we are irritated, we feel the incredible power of the feeling without lashing out at others.

> We become more human because we allow more of ourselves.

> We can end the tyranny of trying to limit, control and deny ourselves who we really are.

> We know that no emotion can actually hurt us, no matter how strong or negative.

> This allows us to feel all of it, to end the life of trying to avoid, hide from, and attempt to fix feelings of discomfort and fear.

> And when we stop trying to control the mind(which we can't control anyway), we allow it to return to calmness and clarity every time it gets caught up in too much thinking.