---
layout: post
title:  "Notes from Quora Articles taken on Tuesday 18-09-2018"
date:   2018-09-18
categories: notes quora personal-development
---

# What should I do in my 30's so I don't regret in my 40's like I regret about my 20's?

#### From http://qr.ae/TUNdJP

---

1. **Take your health seriously**
2. **Save and Invest 20% of your check**
3. **Educate yourself daily**
4. **Start sleeping 8 hours+ a night**
5. **Get a coach**
6. **Take it easy on yourself**
7. **Stop listening to broke/unhappy people**
8. **Give up the Major vices**
9. **Quite doing stuff you hate**
10. **Be present**
11. **Embrace your mortality**