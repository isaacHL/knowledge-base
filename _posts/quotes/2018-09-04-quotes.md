---
layout: post
title:  "Quotes"
date:   2018-09-04
categories: quotes
---

# Quotes

> There is nothing outside of yourself that can never enable you to get better, stronger, richer, quicker or smarter. Everything is within. Everything exists. Seek nothing outside of yourself. 
> *- Miyamoto Musashi*

> Difficult doesn't mean impossible. It simply means that you have to work hard.

> If it scares you, it might be a good thing to try.
> *- Set Godin*

> Pain is temporary, it may last for a minute, for an hour, for a day, or even a year. But eventually it will subside, and something else will take its place. If I quit, however, it will last forever.

> Don't be afraid to fail. You can't always win but don't be afraid of making decisions.

> He who says he can, and he who says he can't, are both usually right.

> Our deepest fear is not that we are inadequate, our deepest fear is that we are powerful beyond measure. It is our light, not our darkness that most frighten us.

> To be able, at any moment, to sacrifice what you are for what you will become. 

> If you're not making someone else's life better, then you're wasting your time.

> At some point you will realize that you have done too much for someone, that the next possible step to do is to stop. Leave them alone, walk away.
> It's not that you're giving up, and it's not like you shouldn't try. It's just that you have to draw the line of determination from desperation. What is truly yours will eventually be yours, and what is not, no matter how hard you try, will never be.

> Try not to become a man of success. But rather try to become a man of value.
> *- Albert Einstein*

> Spend a little more time trying to make something of yourself and a little less time trying to impress people...
> *- The Breakfast Club*

> In the beginner’s mind there are many possibilities, but in the expert’s mind there are few.
> *- Shunryu Suzuki*

> You must first define what you want before you can get it.

> Providing MASSIVE and unparalleled value to people who need it and are willing to pay for it.

> Every moment you live in the past is a moment you waste in the present.
> *- Tony Robbins*

> Change your thoughts and you change your world.
> *- Norman Vincent Peale*