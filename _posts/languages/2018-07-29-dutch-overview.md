# Dutch

> From Duolingo:

Dutch is a Germanic language, with grammar and vocabulary similar to other European languages. Dutch is a language with grammatical genders. These genders have influence on endings on words.

## Gender and Articles

In Dutch, there are three (grammatical) genders: masculine, feminine, and neuter. 

Each gender has their own definite article (‘the’): both singular masculine and feminine nouns use **DE** and singular neuter nouns use **HET**. For plural nouns, **DE** is always used. 

The definite articles **DE** and **HET** don't have very clear rules for when you're supposed to use which; this will mostly be learning by heart and developing a feeling for it. However, there are some guidelines to help you along:

### De Words

* *De* is always used for plural nouns

* *De* is always used for professions: **de kok** (‘the chef’), **de leraar** (‘the teacher’)

* *De* tends to be used for people with an identified gender, such as: **de vader** (‘the father’), **de dochter** (‘the daughter’)

* *De* is used for vegetables, fruits, trees and plants, names of mountains, and rivers

* Furthermore, *De* is used for most words ending on **-ie**, **-ij**, **-heid**, **-teit**, **-schap**, **-tie**, **-sie**, **-aar**, **-eur**, **-er**, and **-or**.

* Finally, *De* is used for written-out numbers and letters: **de drie** (‘the three’), **de a** (‘the a’).

### Het words

* *Het* is always used for diminutives. Diminutives can be recognised by their suffix; they end in **-je**, **-tje**, **-etje**, **-pje**, or **-mpje**.

* *Het* is always used for words consisting of two syllables and starting with **be-**, **ge-**, **ver-**, and **ont-**

* *Het* is always used for verbs used as nouns. When the infinitive form of a verb is used as a noun (e.g. 'the walking of the dog'), Dutch uses het (het lopen van de hond).

* *Het* is always used for languages and names of metals.

* *Het* is also used for names of compass points: **het noorden** (‘the North’)

* *Het* is used for names of sports and games: **het schaken** (‘chess’), **het voetbal** (‘football/soccer’)

* Furthermore, het is used for words ending on **-isme** and **-ment**

Dutch speakers actually never tend to think about the gender of words. Rather than knowing whether a word is originally feminine or masculine, the only distinction that has to be remembered is the difference between the *de* words and *het* words. This is because it has grammatical consequences (in terms of possessives, question words, demonstratives, adjectives, and even relative pronouns). This is why when you learn a new noun, it is very important to memorize whether it is a *de* or *het* word.



### Pronouns

English | Dutch
----|---
I | Ik
You(Singular|Jij(*Je)
He/She/It|Hij/Zij(*Ze)/Het
You (formal) | U
We | Wij(We*)
You (plural) | Jullie
They | Zij(Ze*)

### Verb Conjugation

In Dutch, verbs can be recognized by the ending **-en**. For example, *eten* ('to eat') and *drinken* ('to drink'). Verb conjugation in Dutch can get rather difficult, since there are lots of exceptions. The most basic rule is: find the stem and add the right ending to it. To find the stem of the word, you take the infinitive of the word - the basic form that you can find in the dictionary - and take off the ending, i.e. **-en**. So in the example of 'drinken', ('to *drink*'), the stem would be *drink*-. For the simple present, the conjugation is as follows:

Pronoun | Conjugation | Example
-|-|-
Ik | [stem] | Ik drink (*I drink*)
Jij | [stem]+t | Jij drinkt (*You drink*)
Hij/Zij/Het | [stem]+t | Hij drinkt (*He drinks*)
U | [stem]+t | U drinkt (*You drink*)
Wij | infinitive | Wij drinken (*We drink*)
Jullie | Infinitive | Jullie drinken (*You drink*)
Zij | Infinitive | Zij drinken (*They drink*)

### Irregular Verbs

Dutch also has irregular verbs. There are only 6 verbs that are completely irregular. There are more which aren't entirely regular.

These are the irregular verbs:

1. Hebben (To have)
2. Kunnen (can)
3. Mogen (may)
4. Willen (want)
5. Zijn (to be)
6. Zullen (shall)

The most common of these are *Hebben* and *Zijn*, so here are their conjugations:

| Hebben|Zijn|
-|-
Ik heb | Ik ben
Jij hebt| Jij bent
U hebt/U heeft|U bent
Hij/Zij/Het heeft| Hij/Zij/Het is 
Wij hebben | Wij zijn
Jullie hebben | Jullie zijn
Zij hebben | Zij zijn

### Pronunciation

| Dutch | IPA, Notes|
-|-
A| [ɑ] (short), like in father. [a:] (long), like in car (Australian/New Zealand English)
B|[b], like in bait. At the end of a word: [p]
C | [s] or [k] depending on the vowel after the c
D | [d], like in duck. At the end of a word: [t]
E | [ɛ] (short), like in bed. [e:] (long), like in made. [ə], an ‘uh’ sound, like again; mostly at the end of verbs.