---
layout: post
title:  "Dutch nouns table"
date:   2019-01-01
categories: languages
---

# Nouns table

### De Words

* *De* is always used for plural nouns

* *De* is always used for professions: **de kok** (‘the chef’), **de leraar** (‘the teacher’)

* *De* tends to be used for people with an identified gender, such as: **de vader** (‘the father’), **de dochter** (‘the daughter’)

* *De* is used for vegetables, fruits, trees and plants, names of mountains, and rivers

* Furthermore, *De* is used for most words ending on **-ie**, **-ij**, **-heid**, **-teit**, **-schap**, **-tie**, **-sie**, **-aar**, **-eur**, **-er**, and **-or**.

* Finally, *De* is used for written-out numbers and letters: **de drie** (‘the three’), **de a** (‘the a’).

Het | De | Pronoun | English
-|-|-|-
Het||water|The water
||De|waters|The waters
Het||kind| The child
||De|kinderen| The children
||De|appel| The apple
||De|appels|The apples
Het||sap|The juice
||De|saps|The juices
||De|jongen|The boy
||De|jongens|The boys
||De|melk|The milk
||De|melks|The milks
||De|man|The man
||Den|mannen|The men
Het||brood|The bread
||De|broden|The breads