---
layout: post
title:  "Dutch conjugation table"
date:   2019-01-01
categories: languages
---


# Conjugation

In Dutch, verbs can be recognized by the ending **-en**. For example, *eten* ('to eat') and *drinken* ('to drink'). Verb conjugation in Dutch can get rather difficult, since there are lots of exceptions. The most basic rule is: find the stem and add the right ending to it. To find the stem of the word, you take the infinitive of the word - the basic form that you can find in the dictionary - and take off the ending, i.e. **-en**. So in the example of 'drinken', ('to *drink*'), the stem would be *drink*-. For the simple present, the conjugation is as follows:

Pronoun | Conjugation | Example
-|-|-
Ik | [stem] | Ik drink (*I drink*)
Jij | [stem]+t | Jij drinkt (*You drink*)
Hij/Zij/Het | [stem]+t | Hij drinkt (*He drinks*)
U | [stem]+t | U drinkt (*You drink*)
Wij | infinitive | Wij drinken (*We drink*)
Jullie | Infinitive | Jullie drinken (*You drink*)
Zij | Infinitive | Zij drinken (*They drink*)

# Conjugation Table

Verb | Ik | Jij | Hij/Zij/Het | U | Wij | Jullie | Zij
-|-|-|-|-|-|-|-
Drinken (To Drink) | Drink | Drinkt | Drinkt | Drinkt | Drinken | Drinken | Drinken
Eten (To eat) | Eet | Eet | Eet | Eet | Eten | Eten | Eten
