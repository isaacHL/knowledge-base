# HTML

Hypertext Markup Language



Taken from different sources:

---
You need to be familiar with two kinds of structures when writing HTML. The Big Picture and the Micro Details. 

1. The Big Picture - How blocks and wrappers are used in the website
2. Micro Details - Within each block of content, what is the best way to present each group of information.

Understand how the big picture looks like.

>  https://zellwk.com/


---

Charset - Declared as first thing in the head, before any content

Title - Title of the site is simple and clean. Purpose of Page is first, a separator is used, and ends with title of the site.

CSS - Only one single stylesheet is used (media types declared inside stylesheet), and only served to good browsers. IE 6 and below are served a universal stylesheet.

Body - ID applied to body to allow for unique page styling without any additional markup.

File Paths - Site resources use relative file paths for efficiency. Content file paths are absolute, assuming content is syndicated.

Image Attributes - Images include alternate text, mostly for visually impaired uses but also for validation. Height and width applied for rendering efficiency.

Main Content  First - The main content of the page comes after basic identity and navigation but before any ancillary content like sidebar material.

Appropriate Descriptive Block-Level Elements - Header, Nav, Section, Article, Aside... all appropriately describe the content they contain better than the divs of old.

Hier

> https://css-tricks.com/what-beautiful-html-code-looks-like/